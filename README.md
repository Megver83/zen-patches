# Zen Patches

The [zen-kernel](https://github.com/zen-kernel/zen-kernel) GitHub repository [provides patches](https://github.com/zen-kernel/zen-kernel/releases) for vanilla Linux kernel sources with `SUBLEVEL` 0. This means, they only apply for the first stable version of a generation, update the kernel version and then adds the Zen Kernel changes. These patches only provide the Zen Kernel changes, which will need to be applied to the same vanilla version as the patch.

This is what [these patches](https://pkgbuild.com/~heftig/zen-patches/) where about, but are now discontinued.
