#!/bin/bash
source_dir="$1"
script_dir="$(dirname $(realpath $0))"

(
    cd "$source_dir"
    git fetch --all --tags

    # Create patches only for the v5+ series
    mapfile -t zen_tags < <(git tag -l | grep '^v[5-9]\..*-zen[0-9]')

    for tag in "${zen_tags[@]}"; do
        if ! [ -e "$script_dir/patch-${tag/v}.diff" ]; then
            git diff --stat -p --no-renames --binary "${tag%-zen*}" "$tag" > "$script_dir/patch-${tag/v}.diff"
        fi
    done
)

mapfile -t patches < <(git ls-files --others --exclude-standard)

if [[ "${#patches[@]}" -gt 0 ]]; then
    (
        cd "$script_dir"
        for patch in "${patches[@]}"; do
            git add "$patch"
            git commit -m "add $patch"
        done
        git push
    )
else
    echo "No new patches"
fi
